import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  toDoList = '';
  task = [
    {toDoList: 'Play with children'},
    {toDoList: 'Play with cat'},
    {toDoList: 'Do homework'},
  ];


  onAddTodo(event:Event){
    event.preventDefault();
    this.task.push({
      toDoList: this.toDoList,
    });
  }

  onDeleteTask(i: number){
    this.task.splice(i, 1);
  }

  changeTask(index: number, newTask: string ){
    this.task[index].toDoList = newTask;
  }
}
