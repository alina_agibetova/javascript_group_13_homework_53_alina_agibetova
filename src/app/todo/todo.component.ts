import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  @Input() toDoList = '';
  @Output() delete = new EventEmitter();
  @Output() inputChange = new EventEmitter<string>();
  focus = false;

  OnDeleteClick(){
    this.delete.emit();
  }

  OnToDoInput(event: Event){
    const target = <HTMLInputElement>event.target;
    this.inputChange.emit(target.value);
  }

}
